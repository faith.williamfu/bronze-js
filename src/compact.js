// TODO:
//
// Implement a function called `compact`. This function creates an array with
// all falsey values removed. The values `false`, `null`, `0`, `""`, `undefined`,
// and `NaN` are falsey.
//
// You can refer to unit test for more details.
//
// <--start-
function compact (array) {
  if (array == null) {
    return array;
  } else {
    return array.filter(isNotFalsey => Boolean(isNotFalsey));
  }
}
// --end-->

export default compact;
