class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (inputProducts) {
    if (inputProducts.length === 0) {
      return 0;
    } else {
      const findProducts = [];
      for (let i = 0; i < inputProducts.length; i++) {
        findProducts.push(this.products.filter(id => id.id === inputProducts[i]));
      }
      if (findProducts.flat().length === 0) {
        throw new Error('Product not found.');
      }
      return findProducts.flat().reduce((sum, products) => sum + products.price, 0);
    }
  }
  // --end->
}

export default RecepitCalculator;
